/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.services;

import com.tdx.campaigntools.objects.SMSRequest;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *
 * @author jalfonzo
 */
public class InterMorosidadCP {

    static boolean debugMode = false;

    public static void printDebugMessage(String message) {
        if (debugMode) {
            printMessage(message);
        }
    }

    public static void printMessage(String message) {
        System.out.println("Thread:" + Thread.currentThread().getId() + " INTER MOROSIDAD: " + message);
    }

    public static boolean validarMensaje(String msg, String regex) {
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(msg);
        return matcher.find();
    }

    public static String process(HashMap<String, String> mp, HashMap<String, String> mr, SMSRequest request) throws Exception {
        String responseString;
        String outputString = "";
        String mensajeResponse;
        String debugProperty = mp.get("debug");
        if (debugProperty != null && debugProperty.equalsIgnoreCase("true")) {
            debugMode = true;
        }
        printMessage("Endpoint " + mp.get("endpoint"));
        printMessage("NameSpace " + mp.get("nameSpace"));
        printMessage("Action " + mp.get("action"));
        printMessage("Passport " + mp.get("passport"));
        printMessage("Password " + mp.get("passwd"));
        try {
            String wsURL = mp.get("endpoint");
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            String xmlInput
                    = " <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gxv=\"" + mp.get("nameSpace") + "\">\n"
                    + " <soapenv:Header/>\n"
                    + " <soapenv:Body>\n"
                    + " <gxv:EnvioSMSTedexis.Execute>\n"
                    + " <gxv:Nromovil>" + request.getNumero() + "</gxv:Nromovil>\n"
                    + " <gxv:Par1>" + request.getTexto() + "</gxv:Par1>\n"
                    + " </gxv:EnvioSMSTedexis.Execute>\n"
                    + " </soapenv:Body>\n"
                    + " </soapenv:Envelope>";
            printMessage("XML send: " + xmlInput);
            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = mp.get("action");
            httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=ISO-8859-1");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();
            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            while ((responseString = in.readLine()) != null) {
                outputString = String.valueOf(outputString) + responseString;
            }
            printMessage("XML respuesta: " + outputString);
            StringTokenizer st = new StringTokenizer(outputString, ">");
            ArrayList<String> partes = new ArrayList<>();
            ArrayList<String> partes2 = new ArrayList<>();
            String aux = "";
            while (st.hasMoreTokens()) {
                aux = st.nextToken();
                partes.add(aux);
            }
            String respuesta = partes.get(5);
            StringTokenizer st2 = new StringTokenizer(respuesta, "<");
            while (st2.hasMoreTokens()) {
                aux = st2.nextToken();
                partes2.add(aux);
            }
            String weatherResult = partes2.get(0);
            if (weatherResult.contains("ERROR")) {
                weatherResult = ((String) partes2.get(0)).substring(17, ((String) partes2.get(0)).length());
            }
            printMessage("WS result: " + weatherResult);
            return weatherResult;
            
        } catch (IOException ex) {
            mensajeResponse = mr.get("mensaje_error");
            printMessage("Exception: ");
            ex.printStackTrace();
        }
        return mensajeResponse;
    }
}
